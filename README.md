# PublishedApiDoclet

You probably know the difference between the public API and the published API. 
In the case of Java the former is the set of packages, classes/interfaces, and
class/interface members, that can be accessed by your code without a compiler
error. The later is the set of packages, classes/interfaces, and class/
interface members, that is intended to be used by a third party. While you can
change the public API using the refactorings provided by your IDE, you can't
change the published API without breaking third party code. Therefore the
published API is usually a subset of the public API. There is an elaborate
paper about all that from Martin Fowler: "Public versus Published Interfaces"
(IEEE Software, March/April 2002, p.18f, online available at 
http://martinfowler.com/ieeeSoftware/published.pdf)

Unfortunately javadoc doesn't support the concept of a published API. The
PublishedApiDoclet tries to leverage this deficiency. It acts as a filtering
proxy (aka decorator) for another doclet, that formats the output. The
PublishedApiDoclet hides any Java package, class or class member, that doesn't
belong to the published API from the formatting doclet.

## RefCheckDoclet

Thanks to a request of Stephane Vincent I added a second doclet to the padoclet
package. Its purpose is to search the generated documentation for references to
undocumented (not included in the set of documented items)  java items
(packages, classes, methods, constructors, fields). If the source code of the
undocumented item is available to javadoc, a warning will be generated, about a
reference to an undocumented item.
 
Example: consider a parameter of a documented method has a type, that is not
documented. Then RefCheckDoclet will create a warning.

This helps you, to keep your published documentation self consistent.
Especially if you exclude parts of your public API from your documentation
(i.e. using PublishedApiDoclet), you might find  RefCheckDoclet valuable.
Therefore it is possible to run RefCheckDoclet from within PublishedApiDoclet
just before the formatting doclet.

## Prerequisites

This version of PublishedApiDoclet supports Java 5 and 6.

## Releases

You can download the latest releases from 
https://bitbucket.org/akruis/publishedapidoclet/downloads 

### Version 1.5-0.4

This release supports Java 5 and 6. For Java 2, please use the latest 1.4 
release. The documentation is more or less complete and there are no known 
bugs. The jar-archive contains the compiled classes as well as the source and the
documentation.

Changes:

   * Ported to Java 5. 
   * Added options to filter the elements of annotations and to filter enum constants.

### Version 1.4-0.3

This release fixes one obscure bug and adds a second doclet to the package: 
RefCheckDoclet. The release is for Java 1.4.2 (Java 2).
The documentation is more or less complete and there are no known bugs. The 
jar-archive contains the compiled classes as well as the source and the
documentation.

## License

The PublishedApiDoclet is free software. It may be used and distributed under
the terms of the GNU LGPL. See LICENSE.txt for details. The copyright and the
German Urheberrecht belongs to Anselm Kruis 2006. Commercial support is
available on request from science+computing AG 
(http://www.science-computing.de, mailto:info@science-computing.de). Comments
and patches should be directed to a.kruis@science-computing.de.

Anselm Kruis